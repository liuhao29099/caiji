import { JsonDB, Config } from 'node-json-db'

async function genList(db) {
	try {
		await db.getData('/list')
	} catch (error) {
		await db.push('/list', [])
	}
	return db
}

async function initDB() {
	const db = new JsonDB(new Config('keywords', true, false, '/'))
	await genList(db)
	return db
}

async function saveKey(key) {
	const db = await initDB()
	await db.push('/list[]', key)
}

async function getKeys() {
	const db = await initDB()
	return await db.getData('/list')
}
async function hasKey(key) {
	const db = await initDB()
	const node = (await db.getData('/list')).find(item => item === key)
	return !!node
}

async function checkAndSaveKey(key) {
	if (!(await hasKey(key))) {
		await saveKey(key)
		return true
	}
}

;(async () => {
	checkAndSaveKey('123')
	const res = await hasKey('123')
})()

export {
	saveKey,
	checkAndSaveKey,
	getKeys,
}
