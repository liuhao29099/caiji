import puppeteer from 'puppeteer'
const { checkAndSaveKey } = require('../../database')

const resKeys = []

async function loadPage(browser, page) {
	let listCount = (await page.$$('.tab-bbs-list > tbody > tr > td:first-child > a')).length
	// 遍历获取到的数据

	const getLineVal = async (selector) => {
		const newPagePromise = new Promise(resolve => {
			browser.on('targetcreated', async (target) => {
				const newPage = (await target.page())
				resolve(newPage)
			})
		})
		await page.click(selector)
		await newPagePromise
		await page.waitForSelector('.el-message-box__wrapper')
		const value = await page.evaluate(() => {
			const el = document.querySelector('#details-page > div.header > h2')
			return el?.innerText
		})
		await page.close()
		return value
	}
	console.log(`一共${listCount}个，采集中`)

	for (let i = 10; i < listCount; i++) {
		const val = await getLineVal(
			`.tab-bbs-list > tbody > tr:nth-child(${i + 1}) > td:first-child > a`
		)
		if (!val) continue
		if (await checkAndSaveKey(val)) {
			resKeys.push(val)
			console.log(`${val}保存成功`)
		} else {
			console.log(`${val}已存在`)
		}
		console.log(`已采集${i - 9}个`)
	}
}

export default async function () {
	// 启动浏览器
	const browser = await puppeteer.launch({
		headless: true, // 默认是无头模式，这里为了示范所以使用正常模式
	})

	// 控制浏览器打开新标签页面
	const page = await browser.newPage()
	// 在新标签中打开要爬取的网页
	await page.goto('https://hjgb5.com/article?nodeId=13')
	await page.waitForSelector('.tab-bbs-list > tbody > tr > td:first-child > a')
	await page.click(
		'#app > div:nth-child(5) > div:nth-child(1) > div > div.el-dialog__footer > span > button'
	)

	await page.waitForSelector('#app > div:nth-child(5) > div:nth-child(2)', {
		hidden: true,
	})
	await loadPage(browser, page)

	for (let i = 1; i < 10; i++) {
		await page.click(`.el-pager>li:nth-child(${i + 1})`)
		page.waitForTimeout(3000)
		await loadPage(browser, page)
	}
	browser.close()
}
