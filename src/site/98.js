import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import { replaceAll, saveToFile } from '../utils.js'

import { executablePath } from 'puppeteer'

import { checkAndSaveKey } from '../../database.js'

puppeteer.use(StealthPlugin())

const resKeys = []

async function goToPage(browser, pageIndex) {
	const page = await browser.newPage()
	await page.setExtraHTTPHeaders({
		'Accept-Language': 'zh-CN,zh;q=0.9',
	})
	// Set the language forcefully on javascript
	await page.evaluateOnNewDocument(() => {
		Object.defineProperty(navigator, 'language', {
			get: function () {
				return 'zh-CN'
			},
		})
		Object.defineProperty(navigator, 'languages', {
			get: function () {
				return ['zh-CN', 'zh', 'zh-TW', 'en-US', 'en']
			},
		})
	})

	await page.goto(`https://www.sjzd5478.com/forum-2-${pageIndex}.html`)
	try {
		await page.waitForSelector('tr > th > a.s.xst')
	} catch (error) {
		console.log(error)
		console.log('获取不到dom，抓取失败，跳过')
	}
	const list = Array.from(await page.$$('tr > th > a.s.xst'))
	console.log('抓取成功，开始收集关键词')
	for (const item of list) {
		const val = await item.evaluate(node => node.innerText)
		if (await checkAndSaveKey(val)) {
			const res = replaceAll(val)
			res && resKeys.push(res)
			console.log(`${val}保存成功🥹`)
		} else {
			console.log(`${val}已存在`)
		}
	}
	page.close()
}

async function handleAlert(browser) {
	// 处理确定框
	const page = await browser.newPage()
	await page.goto(`https://www.sjzd5478.com/forum-2-1.html`)
	await page.waitForTimeout(3000)
	await page.waitForSelector('.enter-btn')
	await page.click('.enter-btn')
	await page.waitForTimeout(3000)
	console.log('跳过确定框，开始采集')
	page.close()
}

export default async function (pageSize = 10) {
	const browser = await puppeteer.launch({
		defaultViewport: null,
		headless: true,
		args: ['-lang=zh-CN,zh,zh-TW,en-US,en'],
		executablePath: executablePath(),
	})

	await handleAlert(browser)

	for (let i = 1; i < pageSize + 1; i++) {
		await goToPage(browser, i)
	}
	// 把resKeys写入文件，每行一个
	saveToFile(resKeys)
}
