const puppeteer = require('puppeteer-extra')

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')

puppeteer.use(StealthPlugin())

const { executablePath } = require('puppeteer')

const { checkAndSaveKey } = require('./database')

const resKeys = []

async function goToPage(browser, pageIndex) {
	const page = await browser.newPage()
	await page.setExtraHTTPHeaders({
		'Accept-Language': 'zh-CN,zh;q=0.9',
	})
	// Set the language forcefully on javascript
	await page.evaluateOnNewDocument(() => {
		Object.defineProperty(navigator, 'language', {
			get: function () {
				return 'zh-CN'
			},
		})
		Object.defineProperty(navigator, 'languages', {
			get: function () {
				return ['zh-CN', 'zh', 'zh-TW', 'en-US', 'en']
			},
		})
	})

	await page.goto(`https://91porn.com/v.php?category=rf&viewtype=basic&page=${pageIndex}`)
	console.log(`新开页面${pageIndex}等待10秒`)
	await page.waitForTimeout(10000)
	const list = Array.from(
		await page.$$(
			'#wrapper > div.container.container-minheight > div.row > div > div > div> div > a > span'
		)
	)
  console.log(list)
  console.log('抓取成功，开始收集关键词');

	for (const item of list) {
		const val = await item.evaluate(node => node.innerText)
		if (await checkAndSaveKey(val)) {
			resKeys.push(val)
			console.log(`${val}保存成功`)
		} else {
			console.log(`${val}已存在`)
		}
	}
	page.close()
}

;(async () => {
	const browser = await puppeteer.launch({
		// executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
		defaultViewport: null,
		headless: false,
		// userDataDir: '/Users/wumao/Library/Application Support/Google/Chrome/Default',
		args: ['-lang=zh-CN,zh,zh-TW,en-US,en'],
		executablePath: executablePath(),
	})

	for (let i = 3; i < 10; i++) {
		await goToPage(browser, i)
	}
	console.log('采集完成')
})()
