import inquirer from 'inquirer'
import readline from 'readline'

export async function selectSite(config) {
	const questions = [
		{
			type: 'list',
			name: 'option',
			message: '选择采集的站点：',
			choices: ['98', '91', 'hj'],
		},
	]
	const res = await inquirer.prompt(questions)
	config.site = res.option
}

export async function inputSize(config) {
	return new Promise((resolve, reject) => {
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
		})
		rl.question('请输入采集的页数：(默认10)', answer => {
			config.page = Number(answer) || 10
			rl.close()
			resolve(true)
		})
	})
}

export async function isGenerateFiles(config) {
	const questions = [
		{
			type: 'list',
			name: 'option',
			message: '是否将关键词生成对应文件：',
			choices: ['是', '否'],
			default: '是',
		},
	]
	const res = await inquirer.prompt(questions)
	return res.option == '是'
}

export async function isGenerateTorrent(config) {
	const questions = [
		{
			type: 'list',
			name: 'option',
			message: '是否将对应文件生成zz文件：',
			choices: ['是', '否'],
			default: '是',
		},
	]
	const res = await inquirer.prompt(questions)
	return res.option == '是'
}

export async function isGenerateTorrentFiles(config) {
	const questions = [
		{
			type: 'list',
			name: 'option',
			message: '是否将对应文件生成torrent：',
			choices: ['是', '否'],
			default: '是',
		},
	]
	const res = await inquirer.prompt(questions)
	return res.option == '是'
}
