import path from 'path'
import fs from 'fs'
import { fileURLToPath } from 'url'
import { dirname } from 'path'
import { isExistAndCreate } from './utils.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

let sumTime = {
	time: 0,
	count: 0,
}

function removeEmoji(content) {
	const reg = /[\u4e00-\u9fa5_a-zA-Z0-9,！，\~？【】（）\[\]]+/g
	const result = content.match(reg)
	return result && result.join('')
}

const copyFile = (sourcePath, targetPath, newName) => {
	const sourceFile = fs.readdirSync(sourcePath, { withFileTypes: true })
	sourceFile.forEach(file => {
		const newSourcePath = path.resolve(sourcePath, file.name)
		const newTargetPath = path.resolve(targetPath, file.name)
		const [name, ext] = file.name.split('.')
		fs.copyFileSync(newSourcePath, newTargetPath)
		if (name === 'origin') {
			fs.renameSync(
				path.resolve(targetPath, `${name}.${ext}`),
				path.resolve(targetPath, `${newName}.${ext}`)
			)
		}
	})
}

async function generateFile(params) {
	params = removeEmoji(params)
	const oldTime = new Date().getTime()
	const targetPath = path.join(__dirname, '../', 'res', params)
	const oldPath = path.join(__dirname, '../', 'pkg')
	const isFileExist = isExistAndCreate(targetPath)
	!isFileExist && copyFile(oldPath, targetPath, params)
	const newTime = new Date().getTime()
	if (isFileExist) {
		console.log(`${params}已存在🤪`)
	} else {
		const time = newTime - oldTime
		sumTime.time += time
		sumTime.count++
		console.log(`${params}已生成, 耗时${time}ms`)
	}
	return true
}

export default async function generateFiles() {
	const resPath = path.resolve(__dirname, '../', 'res')
	isExistAndCreate(resPath)
	const keywordData = fs.readFileSync(path.resolve(__dirname, '..', 'keys.txt'), 'utf-8')
	const keys = keywordData.split(/\n/)
	for (const key of keys) {
		await generateFile(key)
	}
	console.log(`总耗时${sumTime.time}ms,生成${sumTime.count}个文件`)
}

