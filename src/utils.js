import fs from 'fs'

const exprList = [
	/[\u{1F600}-\u{1F64F}]|[\u{1F300}-\u{1F5FF}]|[\u{1F680}-\u{1F6FF}]|[\u{2600}-\u{26FF}]|\u{3299}|\u{2764}\u{FE0F}/gu,
	/2023年最新安卓APP，建议全体重新下载【修复报毒的问题】优化了很多东西/g,
	/最新永久访问本站方法和安卓APP和IOS【请大家重新保存】最新的三天换一次网址/g,
	/二次验证丢失和禁止申诉用户想申诉进/g,
	/想得到邀请码的进【最新规则】/g,
	/^\d{4}-\d+-\d+/g,
	/\((上|中|下)\)/g,
	/（(上|中|下)）/g,
]

// 挨个正则匹配，匹配到的数据清空

export function replaceAll(str) {
	exprList.forEach(expr => {
		str = str.replace(expr, '')
	})
	return str
}

export function saveToFile(resKeys) {
	// 把resKeys写入文件，每行一个

	//如果没有backup目录，创建一个
	if (!fs.existsSync('./backup')) {
		fs.mkdirSync('./backup')
	}
	//如果老文件存在，先备份到一个备份目录中再删除
	if (fs.existsSync('./keys.txt')) {
		const date = new Date()
		const year = date.getFullYear()
		const month = date.getMonth() + 1
		const day = date.getDate()
		const hour = date.getHours()
		const minute = date.getMinutes()
		const second = date.getSeconds()
		const time = `${year}-${month}-${day}-${hour}-${minute}-${second}`
		fs.renameSync('./keys.txt', `./backup/${time}.txt`)
	}

	fs.writeFileSync('./keys.txt', resKeys.join('\n'))
	console.log('关键词采集成功')
}

export function isExistAndCreate(path) {
	// 判断文件夹是否存在, 不存在创建一个
	if (!fs.existsSync(path)) {
		fs.mkdirSync(path)
		return false
	}
	return true
}
