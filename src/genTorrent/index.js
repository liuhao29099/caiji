import { fileURLToPath } from 'url'
import { dirname } from 'path'
import createTorrent from 'create-torrent'
import fs from 'fs'
import path from 'path'
import { isExistAndCreate } from '../utils.js'
import dayjs from 'dayjs'
import config from './config.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

export default async function generateTorrent() {
	// 父文件夹路径和种子文件保存路径
	const parentFolderPath = path.resolve(__dirname, '../', '../', 'res')
	const outputFolderPath = path.resolve(__dirname, '../', '../', 'torrentRes')

	isExistAndCreate(outputFolderPath)
	isExistAndCreate(parentFolderPath)

	// 根据生成时间备份老的种子文件
	const oldTorrentResPathBackup = path.resolve(
		__dirname,
		'../',
		'../',
		'backup',
		`torrentResBackup`
	)

	isExistAndCreate(oldTorrentResPathBackup)
	fs.renameSync(
		outputFolderPath,
		path.resolve(oldTorrentResPathBackup, dayjs().format('YYYY-MM-DD'))
	)

	isExistAndCreate(outputFolderPath)

	// 遍历文件夹并创建种子文件
	fs.readdir(parentFolderPath, async (err, items) => {
		if (err) {
			console.error('读取父文件夹失败:', err)
			return
		}
		for (const item of items) {
			const itemPath = path.join(parentFolderPath, item)

			// 检查是否是文件夹
			if (fs.statSync(itemPath).isDirectory()) {
				try {
					// 创建种子文件
					const torrent = await createTorrentPromise(itemPath, {
						...config,
					})
					const torrentFilePath = path.join(outputFolderPath, `${item}.torrent`)

					// 保存种子文件
					fs.writeFileSync(torrentFilePath, torrent)
					console.log(`zz文件已生成`)
				} catch (error) {
					console.error(`创建zz文件失败: ${itemPath}`, error)
				}
			}
		}
	})

	// 将 createTorrent 转换为返回 Promise 的函数
	function createTorrentPromise(folderPath) {
		return new Promise((resolve, option, reject) => {
			createTorrent(folderPath, (err, torrent) => {
				if (err) {
					reject(err)
				} else {
					resolve(torrent)
				}
			})
		})
	}
}
