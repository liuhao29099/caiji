import { selectSite, inputSize, isGenerateFiles, isGenerateTorrent } from './src/question.js'
import start98 from './src/site/98.js'
import generateFiles from './src/generateFiles.js'
import generateTorrent from './src/genTorrent/index.js'

const config = {
	site: '98',
	page: 10,
}

function startCollect(config) {
	const collectFn = {
		98: start98,
	}
	console.log(`开始采集${config.site}的数据，共${config.page}页`)
	return collectFn[config.site](config.page)
}

;(async () => {
	if (await isGenerateTorrent()) {
		await generateTorrent()
	}
})()
